<footer>
    <div class=" panel-footer footer-bottom">
        <div class="container-inverse text-center">
            <p> Copyright © Mavis Ong. All right reserved. <em>
                    <?php
                    date_default_timezone_set('Asia/Kuala_Lumpur');
                    print date('g:i a l F j');
                    ?>
                </em></p>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>

<!-- Custom Theme Scripts -->
<script src="../build/js/custom.min.js"></script>
